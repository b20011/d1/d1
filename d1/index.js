let students = ['John', 'Joe', 'Jane', 'Jessie'];

console.log(Math.PI);
//Will round up or down to the neared int
console.log(Math.round(3.14));
//Will always round up to the nearest integer
console.log(Math.ceil(3.14));
//Rounds down to the nearest int
console.log(Math.floor(3.14));
//Returns only the integer part of the number
console.log(Math.trunc(3.14));

console.log(students[Math.trunc(Math.random() * students.length)]);
