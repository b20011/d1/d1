let students = ['John', 'Joe', 'Jane', 'Jessie'];
const errorMessage = 'error - can only add strings to an array';

//How do you create arrays in JS?
// const arrTest = [];

//How do you access the first character of an array?
// students[0];

//How do you access the last character of an array?
// students[students.length - 1];

//What array method searches for, and returns the index of a given value in an array? This method returns -1 if given value is not found in the array.
// students.findIndex('John');

//What array method loops over all elements of an array, performing a user-defined function on each iteration?
//forEach?

//What array method creates a new array with elements obtained from a user-defined function?
// console.log(students.map((x) => x + ' j'));

//What array method checks if all its elements satisfy a given condition?
// console.log(students.every((x) => x.length >= 4));

//What array method checks if at least one of its elements satisfies a given condition?
// console.log(students.some((x) => x.length <= 4));

//True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
//False

//True or False: array.slice() copies elements from original array and returns them as a new array.
//True

// Create a function named addToEnd that will add a passed in string to the end of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing.

const addToEnd = (str) => {
  if (typeof str !== 'string') {
    return errorMessage;
  } else {
    students.push(str);
    return students;
  }
};

// console.log(addToEnd('Ryan'));

//Create a function named addToStart that will add a passed in string to the start of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing.
const addToStart = (str) => {
  if (typeof str !== 'string') {
    return errorMessage;
  } else {
    students.unshift(str);
    return students;
  }
};

// console.log(addToStart('Tess'));

//Create a function named elementChecker that will check a passed in array if at least one of its elements has the same value as a passed in argument. If array is empty, return the message "error - passed in array is empty". Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Jane" as arguments when testing.
const elementChecker = (arr, str) => {
  if (arr.length === 0) {
    return 'error - passed in array is empty';
  }

  const found = arr.find((element) => element === str);

  return found ? true : false;
};

elementChecker(students, 'Jane');

/*
Create a function named checkAllStringsEnding that will accept a passed in array and a character. The function will do the ff:


if array is empty, return "error - array must NOT be empty"
if at least one array element is NOT a string, return "error - all array elements must be strings"
if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
if every element in the array ends in the passed in character, return true. Otherwise return false.

Use the students array and the character "e" as arguments when testing.
*/
const checkAllStringsEnding = (arr, char) => {
  if (arr.length === 0) {
    return 'error - array must NOT be empty';
  }

  if (typeof char !== 'string') {
    return 'error - 2nd argument must be of data type string';
  } else if (char.length > 1) {
    return 'error - 2nd argument must be a single character';
  }

  const arrayChecker = arr.every((element) => typeof element === 'string');
  if (!arrayChecker) return 'error - all array elements must be strings';

  const checker = arr.every(
    (element) =>
      element[element.length - 1].toLocaleLowerCase() ===
      char.toLocaleLowerCase()
  );
  return checker;
};

// console.log(checkAllStringsEnding(students, 'E'));

//Create a function named stringLengthSorter that will take in an array of strings as its argument and sort its elements in an ascending order based on their lengths. If at least one element is not a string, return "error - all array elements must be strings". Otherwise, return the sorted array. Use the students array to test.

const stringLengthSorter = (arr) => {
  return arr.sort((a, b) => a.length - b.length);
};

// console.log(stringLengthSorter(students));

/*
Create a function named startsWithCounter that will take in an array of strings and a single character. The function will do the ff:

if array is empty, return "error - array must NOT be empty"
if at least one array element is NOT a string, return "error - all array elements must be strings"
if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
return the number of elements in the array that start with the character argument, must be case-insensitive

Use the students array and the character "J" as arguments when testing.
*/
const startsWithCounter = (arr, char) => {
  const charToLower = char.toLowerCase();
  if (arr.length === 0) {
    return 'error - array must NOT be empty';
  }

  if (typeof charToLower !== 'string') {
    return 'error - 2nd argument must be of data type string';
  } else if (charToLower.length > 1) {
    return 'error - 2nd argument must be a single character';
  }

  const arrayChecker = arr.every((element) => typeof element === 'string');
  if (!arrayChecker) return 'error - all array elements must be strings';

  let counter = 0;

  arr.forEach((el) => {
    if (el[0].toLowerCase() === charToLower) {
      counter++;
    }
  });
  return counter;
};

// console.log(startsWithCounter(students, 'J'));

//Create a function named randomPicker that will take in an array and output any one of its elements at random when invoked. Pass in the students array as an argument when testing.
const randomPicker = (arr) => {
  return arr[Math.trunc(Math.random() * arr.length - 1)];
};

console.log(randomPicker(students));
